def get_numbers(numbers,error):
        selection = set()
        invalid = set()
        # tokens are comma separated values
        tokens = [x.strip() for x in numbers.split(',')]
        for i in tokens:
            if len(i) > 0:
                if i[:1] == "<":
                    i = "1-%s"%(i[1:])
            try:
                # typically tokens are plain old integers
                selection.add(int(i))
            except:
                # if not, then it might be a range
                try:
                    token = [int(k.strip()) for k in i.split('-')]
                    if len(token) > 1:
                        token.sort()
                    # we have items seperated by a dash
                    # try to build a valid range
                        first = token[0]
                        last = token[len(token)-1]
                        for x in range(first, last+1):
                            selection.add(x)
                except:
                    # not an int and not a range...
                    invalid.add(i)
        # Report invalid tokens before returning valid selection
        if len(invalid) > 0:
            error.insert("end-1c","Invalid set: " + str(invalid)+'\n')
        ids=[]
        for index in selection:
            ids.append(index)
   
        return ids