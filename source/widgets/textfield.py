from tkinter import *
from tkinter import ttk

from widgets.tooltip import ToolTip, ToolTipBase

class TextField(Frame):
    def __init__(self, page, text, R, C, default, tip):
        Frame.__init__(self)
        self.label=Label(page, text=text)
        self.label.grid(row=R, column=C, sticky=W)
        self.val = Entry(page)
        self.val.insert(END, default)
        self.val.grid(row=R+1, column=C, sticky=W)
        if tip!=None:
            self.ttp=ToolTip(self.label, tip)
        
    def get(self):
        return self.val.get()
        
    def insert(self,value):
        self.val.insert(END, value)
        
    def replace(self, value):
        self.val.delete(0,END)
        self.val.insert(END, value)
        
    def change_label(self, labelText, tooltip):
        self.label.config(text=labelText)  
        self.ttp.text=tooltip
        
    def make_inactive(self):
        self.val.config(state=DISABLED,highlightbackground='gray')
    
    def make_active(self):
        self.val.config(state=NORMAL,highlightbackground='white')
        
        