"""A script to generate initial positions muon positions in a symmetry-reduced
subspace of the unit cell"""

import os, shutil, sys
import glob
import spglib as spg
import numpy as np
from numpy import linalg as LA
from argparse import ArgumentParser
from ase import io, Atoms
from ase.io import write
from ase.calculators.castep import Castep
from ase.build.supercells import make_supercell
from ase.geometry import cell_to_cellpar
from ase.spacegroup import crystal
from tkinter import simpledialog
from tkinter import messagebox
from tkinter import ttk
from ase.visualize import view
from custom_view import *

import networkx as nx

from config import *

# function to generate symmetry equivalent positions
def gen_sym_equiv(basis, structure, label):
    spacegroup=spg.get_symmetry_dataset(structure)
    replicate = crystal(label, basis,spacegroup=spacegroup['number'], 
    cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate
 
# function to generate single muon in cell
def gen_single_muon(basis, structure, label):
    replicate = crystal(label, basis, spacegroup=1,
    cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate

def gen_custom(input_struct,vdws,outputtext):
    muon_sites=[]
    num=1
    while True:    
            a = simpledialog.askstring("Input",'Enter fractional coordinates for initial muon position %i.' 
            ' Type "done" if finished.\n' %num)
            if a=='done':
                break
            else:
                try:
                    new_pos=input_struct.cell @ [float(x) for x in a.split()]
                    check_struct=input_struct.copy()+Atoms('Am', positions=[new_pos])
                    atoms1 = [x for x in range(len(check_struct.get_atomic_numbers())) 
                    if check_struct.get_chemical_symbols()[x]!='Am']
                    atoms2=[x for x in range(len(check_struct.get_atomic_numbers()))
				    if check_struct.get_chemical_symbols()[x]=='Am']
                    distances_check=(np.min(check_struct.get_distances(atoms1, atoms2[0],mic=True)))
                    if distances_check>=vdws:
                        muon_sites.append(LA.inv(input_struct.cell) @ new_pos)
                        num+=1
                    else:
                        messagebox.showerror("Error", 'Error: Muon must be at least %s angstroms from all other'
                        ' atoms in cell.\n' %vdws)
                      
                except ValueError:
                    outputtext.insert("end-1c",'Coordinates entered incorrectly. Please try again.\n')
    return muon_sites
   
def gen_random(input_struct, minr, vdws, muon_sites, outputtext):
    outputtext.insert("end-1c",'Generating random configurations with the muons being at least %s angstroms'
        ' from each other and at least %s angstroms away from other atoms in the cell...\n' %(minr, vdws))
    attempts=0 # counts number of times we try to add a new muon but fail
    muon_sites.append(np.random.random((3,)))
    while attempts<30:
        distances=[]
        distances_struct=[]
        fp=np.random.random((3,))
        for pos in muon_sites:
            old=gen_single_muon(pos, input_struct, 'Am') 
            # hopefully your sample doesn't have Americium in it!
            new=gen_sym_equiv(fp, input_struct, 'Es') 
            # ...or Einsteinium.
            combined=old+new+input_struct # put muon sites into structure
            atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[x]=='Am']
            atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[y]=='Es']
            atoms3 = [z for z in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[z]!='Am'
		    and combined.get_chemical_symbols()[z]!='Es']
            for k in range(len(atoms2)):
                distances.append(np.min(combined.get_distance(atoms1[0], atoms2[k], mic=True)))    
            distances_struct.append(np.min(combined.get_distances(atoms3, atoms2[0], 
                    mic=True)))
        if np.min(distances)>=minr and np.min(distances_struct)>=vdws:
            muon_sites.append(fp)
            #print("sites generated : %i" %len(muon_sites))
        else:
            attempts+=1
            #print("failed attempts : %i" %attempts)

    outputtext.insert("end-1c",'{0} total configurations.\n'.format(len(muon_sites)))
    return muon_sites
    
def gen_cluster(input_struct,muon_sites, tol,outputtext):   
    outputtext.insert("end-1c",'Shifting muon positions to occupy smaller '
     'subspace of the unit cell...\n')
    # construct distance matrix  
    dist_mat=[]
    for site_j in muon_sites:    
            replica_j=gen_sym_equiv(site_j, input_struct, 'H')
            for site_i in muon_sites:
                replica_i=gen_single_muon(site_i, input_struct, 'O')
                combined=replica_i+replica_j
                atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
                if combined.get_chemical_symbols()[x]=='O']
                atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
                if combined.get_chemical_symbols()[y]=='H']
                distances=[]
                for k in range(len(atoms2)):
                    distances.append(combined.get_distances(atoms1, atoms2[k], mic=True))
                dist_mat.append(np.min(distances))

    dist_mat=np.reshape(dist_mat, (len(muon_sites), len(muon_sites)))

    # construct adjacency matrix
    adj=dist_mat < float(tol)
    adj[np.diag_indices_from(adj)]=0
    graph=nx.from_numpy_matrix(adj.astype(int))

    # find connected subgraphs
    subgraphs=[]
    for h in nx.connected_component_subgraphs(graph):
        subgraphs.append(list(h.nodes)) 

    # shift members of each subgraph to symmetry equivalent position that is closest
    # to the origin but still inside cell
    shifted=[]
    for subgraph in subgraphs:
        # find member of each subgraph that is closest to the origin, but still inside cell
        dist_origin=[]
        sub_muon_sites=[muon_sites[p] for p in subgraph]
        for site in sub_muon_sites:
            replica_i=gen_sym_equiv(site, input_struct, ['O'])
            distances=[]
            for single_mu in replica_i: 
                muon_cart=single_mu.position
                if all(coord>=0 for coord in muon_cart):
                    distances.append(LA.norm(muon_cart))
            dist_origin.append(min(distances))
        origin_index=np.argmin(dist_origin)

	    # translate each member of subgraph to be closest to the member that 
	    # is closest to the origin
    
        replica_1=gen_sym_equiv(muon_sites[origin_index], input_struct, ['H']) 
        distances=[]
        muon_cart_pos=[]
        for single_mu in replica_1:   
                muon_cart=single_mu.position 
                if all(coord>=0 for coord in muon_cart):
                    muon_cart_pos.append(muon_cart)
                    distances.append(LA.norm(muon_cart))
        muon_frac_pos=LA.inv(replica_1.cell) @ muon_cart_pos[np.argmin(distances)]
        replica_1=gen_sym_equiv(muon_frac_pos, input_struct, ['H'])
        sub_muon_sites=[muon_sites[p] for p in subgraph]
        for site_i in sub_muon_sites:
            replica_i=gen_sym_equiv(site_i, input_struct, ['O'])
            combined=replica_i+replica_1
            atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[x]=='O']
            atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[y]=='H']
            distances=combined.get_distances(atoms1, atoms2[0], mic=False)[::3]
            shifted.append(replica_i.get_positions()[np.argmin(distances)])
          
    tmp_sites=[]
    for mupos in shifted:
             tmp_sites.append(LA.inv(input_struct.cell) @ mupos)
    muon_sites=tmp_sites
    return muon_sites

def create_input(input_struct,muon_sites,musym,folder,rootname,scell_matrix, outputtext):
    scaled=[]
    for pos in muon_sites:
        scaled.append(input_struct.cell @ pos)
    
    # Muon mass and gyromagnetic ratio
    mass_block = 'AMU\n{0}       0.1138000000'
    gamma_block = 'radsectesla\n{0}        851586494.1'

    # Check if chosen symbol for muon conflicts with elements in unit cell
    if musym in input_struct.get_chemical_symbols():
        outputtext.insert("end-1c",'WARNING: chosen muon symbol already appears in'
              ' unit cell.\n')
 
    # Create a CASTEP calculator
    ccalc = Castep()
    ccalc.cell.species_mass = mass_block.format(musym).split('\n')
    ccalc.cell.species_gamma = gamma_block.format(musym).split('\n')
    ccalc.cell.fix_all_cell=True
        
    if len(scaled)>0:
        # generate supercell
        sm = np.array([[float(j) for j in i] for i in scell_matrix]).reshape((3, 3))
        scell0 = make_supercell(input_struct, sm)

        # Clean up any existing structures
        try:
            shutil.rmtree(folder)
        except OSError:
            pass
        os.mkdir(folder)

        # Create the supercell versions and save them
        for i, mupos in enumerate(scaled):
            # Add the muon to the structure
            scell = scell0.copy() + Atoms('H', positions=[mupos])
            # Add castep custom species
            csp = scell0.get_chemical_symbols() + [musym]
      
            scell.set_array('castep_custom_species', np.array(csp))
            scell.set_calculator(ccalc)
          
            io.write(os.path.join(folder, '{0}_{1}.cell'.format(rootname,
                                                              i+1)),
                   scell)
        # view all muon positions within the supercell
        all_muons=scell0.copy()
        for mupos in scaled:
            # Add the muon to the structure
            all_muons +=Atoms('H', positions=[mupos])
        final_struct[0]=all_muons
        outputtext.insert("end-1c",'Done! Click "View initial muon positions" to see sites generated.\n')
        #custom_view(all_muons)
     
    else:
        outputtext.insert("end-1c",'No muon positions!\n')
    
def gen_main(struct, muon_sites, musym, minr, vdws, 
            tol,folder, rootname, random, sym_red,close_to, element, scell_matrix, 
            outputtext,final_struct):
    if random==True:
        muon_sites=gen_random(struct, minr, vdws, muon_sites, outputtext)
        
    if sym_red==True:
        muon_sites=gen_cluster(struct,muon_sites, tol, outputtext)
    
    if close_to==True:
        muon_sites=gen_closeto(struct, element, vdws, muon_sites, outputtext)
    
    create_input(struct,muon_sites,musym,folder,rootname,scell_matrix, outputtext)
    

def gen_closeto(input_struct, element, vdws, muon_sites, outputtext):
    
    outputtext.insert("end-1c","Adding muon positions that are %s angstroms from each of "
    "the %s atoms in the unit cell.\n" %(vdws,element))
    
    
    elem_pos=[atom.position for atom in input_struct if atom.symbol==element]
    
    for atom_pos in elem_pos:
    
        valid_pos=False
        
        while valid_pos==False:
            # generate random vector with length equal to vdws
            vec = np.random.randn(1, 3)[0]
            vec /= np.linalg.norm(vec, axis=0)
            vec *= vdws

            # convert position of atom to cartesians and add vector
            new_pos=vec+atom_pos
        
            check_struct=input_struct.copy()+Atoms('Am', positions=[new_pos])
            # remove atoms that we want to be closer to
            del check_struct[[atom.index for atom in input_struct if atom.symbol==element]]
            atoms1 = [x for x in range(len(check_struct.get_atomic_numbers())) 
            if check_struct.get_chemical_symbols()[x]!='Am']
        
            atoms2=[x for x in range(len(check_struct.get_atomic_numbers()))
		    if check_struct.get_chemical_symbols()[x]=='Am']
        
            distances_check=(np.min(check_struct.get_distances(atoms1, atoms2[0],mic=True)))
            if distances_check>=1.6*vdws: 
            #factor 1.6 reflects typical coordination geometries
                muon_sites.append(LA.inv(input_struct.cell) @ new_pos)
                valid_pos=True
                     
    return muon_sites
    