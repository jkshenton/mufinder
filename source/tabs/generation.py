from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import tkinter.scrolledtext as tkst

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.table import SimpleTableInput

from ase import io
import os
import threading

from custom_view import *
from sitegen.gen_sites import *

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.table import SimpleTableInput
from widgets.textfield import TextField

from config import *

class generation(ttk.Notebook):  
     
    def __init__(self, *args, **kwargs):
        
        def set_struct():
            try:
                input_struct=filedialog.askopenfilename()
                input.delete(0, 'end')
                input.insert(END, os.path.normpath(input_struct))
                struct=io.read(input_struct)
                atom_list=list(set(struct.get_chemical_symbols()))
                menu = atom_choice["menu"]
                menu.delete(0, "end")
                for string in atom_list:
                    menu.add_command(label=string, 
                             command=lambda value=string: self.om_variable.set(value))
                outputtext.insert("end-1c","%s loaded as input cif file.\n" %input_struct)
                input_root=os.path.basename(input.get()).replace('.cif', '')
                bad_chars="() "
                for c in bad_chars:
                    input_root = input_root.replace(c,"")
                rootname.replace(input_root)
                try:
                    custom_view(struct) 
                except UnicodeDecodeError:
                    pass      
            except: 
                outputtext.insert("end-1c","Error loading file.  Please try again.\n")
        
        def run_gen():  
            global final_struct
            struct=io.read(input.get())
            if custom.get()==True:
                if random.get()==True:
                   outputtext.insert("end-1c",'Enter user-defined muon positions.  Random muon positions will be '
           'generated afterwards.\n')
                muon_sites=gen_custom(struct,float(vdws.get()),outputtext)
            else:
                muon_sites=[]
            inputf_full=os.path.join(os.path.dirname(input.get()),inputf.get())
            thread=threading.Thread(target=gen_main, args=(struct, muon_sites, musym.get(), float(minr.get()), float(vdws.get()), 
            tol.get(),inputf_full, rootname.get(), random.get(), sym_red.get(),closeto.get(), self.om_variable.get(), scell_matrix.get(), 
            outputtext,final_struct))
            thread.start()
            #custom_view(final_struct[0])
   
        def view_struct():
            if final_struct==[None]:
                outputtext.insert("end-1c","Site generation not yet run.\n")
            else:
                custom_view(final_struct)
             
        ttk.Frame.__init__(self, *args, **kwargs)
         
        Label(self, text="Input structure:").grid(row=0, column=0, sticky=W)
        input = Entry(self)
        input.grid(row=1, sticky=W)
   
        b = ttk.Button(self, text="load", width=10, command=set_struct).grid(row=2, column=0, sticky=W)

        inputf=TextField(self, "folder name", 3, 0, 'input',
        'folder to contain input files for calculations')
        
        rootname=TextField(self, "rootname", 5, 0, '',
        'Root name for input files')

        Label(self, text="Mode:").grid(row=7, column=0, sticky=W)
        custom = IntVar()
        Checkbutton(self, text="user-defined", variable=custom).grid(row=8, column=0, sticky=W)
        random = IntVar()
        rand=Checkbutton(self, text="randomly generated", variable=random)
        rand.grid(row=9, column=0, sticky=W)
        rand.select()

        sym_red = IntVar(self)
        sym_tick=Checkbutton(self, 
        text="Relocate muon sites to symmetry-reduced subspace?", variable=sym_red)
        sym_tick.grid(row=10, column=0, sticky=W)
        sym_tick.select()
        
        atom_list=['']
        self.om_variable = StringVar(self)
        self.om_variable.set(atom_list[0])
        
        closeto = IntVar(self)
        closeto_tick=Checkbutton(self, 
        text="Attach muon to element", variable=closeto)
        closeto_tick.grid(row=11, column=0, sticky=W)
        
        atom_choice=ttk.OptionMenu(self, self.om_variable, atom_list[0], *atom_list)
        atom_choice.grid(row=11, column=0, sticky=E)

        musym=TextField(self, "Symbol for muon", 0, 1, 'H:mu',
        'symbol used to represent muon in calculations')
        
        minr=TextField(self, "minr", 2, 1, '0.5',
        "Minimum distance between initial muon positions")
    
        vdws=TextField(self, "VDW scale", 4, 1, '1.0',
        "Minimum distance between initial muon position and atoms in the structure")
        
        tol=TextField(self, "Tolerance for clustering", 6, 1, '1.0',
        'Maximum distance between muon positions for them to be considered "connected"')
    
        variable = StringVar(self)
        variable.set("none") # default value
        Label(self, text="Supercell matrix").grid(row=8, column=1, sticky=W)

        scell_matrix=SimpleTableInput(self, 3, 3)
        scell_matrix.grid(row=9, column=1, rowspan=3, sticky=W)

        outputtext = tkst.ScrolledText(self, height=10, width=100)
        outputtext.grid(column=0, row=14, columnspan=2)

        ttk.Button(self, text='Run', command=run_gen).grid(row=12, column=0, sticky=W, pady=4)

        b_view=ttk.Button(self, text='View initial muon positions', command=view_struct)
        b_view.grid(row=15, column=0, sticky=W, pady=4)
        self.grid_rowconfigure(16, weight=1)
        ttk.Button(self, text='Quit', command=self.quit).grid(row=17, column=0, sticky=W, pady=4)
    


  
        
        