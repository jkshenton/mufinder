from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import tkinter.scrolledtext as tkst

from ase import io
from functools import partial

import threading
import shutil
from shutil import copyfile
import os
import re
import multiprocessing
import glob

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.textfield import TextField
from widgets.get_numbers import get_numbers
from widgets.table import SimpleTableInput
from widgets.multi_dialog import CustomDialog

from runtools.write_input import write_kpoints,write_to_cell, param_text

from hpc.write_script import write_script
from hpc.ssh import ssh, scp, check_password, copy_queue, search_queue, submit_job
import shlex

import tornado
import ase.io.cif
import ase.gui.ag

import pickle

from config import *
from custom_view import *

castep_calc=[]

# Generate key
from cryptography.fernet import Fernet
key = Fernet.generate_key()
cipher_suite = Fernet(key)

class run_CASTEP(ttk.Notebook):  

    def __init__(self, *args, **kwargs):
         
        self.jobs=[]
        self.sites=[]
        self.rootname=[]
        self.hpc_address=''
        self.submission_sys=''
        self.password=None
        self.cores_per_node=24
        
        self.old_param=1
        self.custom_p=None
        self.custom_cell=''
         
        def load_dir():
            cells_folder=filedialog.askdirectory()
            cells.delete(0, 'end')
            cells.insert(END, os.path.normpath(cells_folder))
                        
        def run_calc():
            if self.mode.get()=='local':
                thread=threading.Thread(target=run_local)
                thread.start()

            if self.mode.get()!='local':
                if self.password==None:
                    self.password=get_password()
                thread=threading.Thread(target=run_cluster)
                thread.start()

        def run_local():
            global castep_calc
            global sites
            sites=get_numbers(inputnums.get(),outputtext)
            input_files = glob.glob(os.path.join(cells.get(), '*.cell'))
            input_index=[int(re.split('[_ .]',os.path.basename(f))[-2]) for f in input_files]
            input_files=[x for _,x in sorted(zip(input_index,input_files))]
            results=os.path.join(os.path.dirname(cells.get()),'results')
            if os.path.isdir(results):
                shutil.rmtree(results)
            os.mkdir(results)
            param_files=[]
            for fname in input_files:
                param_files.append(fname.replace('.cell', '.param'))
            for j in sites:
                folder=os.path.join(cells.get(),'site%i' %j)
                if os.path.isdir(folder):
                    shutil.rmtree(folder)
                os.mkdir(folder)
                copyfile(input_files[j-1], os.path.join(folder,os.path.basename(input_files[j-1])))
                # add k points if specified
                write_to_cell(self.custom_cell,os.path.join(folder,os.path.basename(input_files[j-1])),self.kpoints.get())
                os.chdir(folder)
                param_files2=os.path.join(folder,os.path.basename(param_files[j-1]))
                if test_custom():
                    ptext=self.custom_p 
                else:
                    ptext=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get()) 
                with open(param_files2,'w') as f:
                    f.write(ptext)
    
                if castep_comm.get()=='castep.mpi':
                    version = subprocess.check_output([mpi_exec, "--version"])
                    if 'Open MPI' in str(version):
                        oversub=' --oversubscribe '
                    else:
                        oversub=' '
                    run_command="%s%s-n %s %s %s" %(mpi_exec,oversub,multiprocessing.cpu_count(),
                castep_mpi,os.path.basename(param_files[j-1]).rstrip(".param"))
                else:
                    run_command="%s %s" %(castep_serial,
                    os.path.basename(param_files[j-1]).rstrip(".param"))
                outputtext.insert("end-1c","Running calculation for site %s.\n" %j)
                castep_calc=subprocess.Popen(shlex.split(run_command), stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                exit_code=castep_calc.wait()
                try:
                    castep_file = glob.glob(os.path.join(folder, '*-out.cell'))[0]
                    output_cell=glob.glob(os.path.join(folder, '*.castep'))[0]
                    copyfile(castep_file, os.path.join(results,os.path.basename(castep_file)))
                    copyfile(output_cell, os.path.join(results,os.path.basename(output_cell)))
                    outputtext.insert("end-1c","Done!\n")
                except:
                    outputtext.insert('end-1c',"Calculation for site %s incomplete.\n" %j)
         
        def get_password():
            address='%s@%s' %(self.username.get(),self.hpc_address)
            while True:
                temp_pass=cipher_suite.encrypt(simpledialog.askstring("Password", "Enter %s password:" %self.mode.get(), show='*').encode())
                if check_password(address,temp_pass,cipher_suite):
                    return temp_pass
                                                               
        def run_cluster():
            address='%s@%s' %(self.username.get(),self.hpc_address)
            pw=self.password #store password in memory
            job_ids=[]
            sites=get_numbers(inputnums.get(),outputtext)
            input_files = glob.glob(os.path.join(cells.get(), '*.cell'))
            input_index=[int(re.split('[_ .]',os.path.basename(f))[-2]) for f in input_files]
            input_files=[x for _,x in sorted(zip(input_index,input_files))]
            input_index.sort()
            self.rootname=os.path.basename(input_files[0]).replace('_%i.cell' %input_index[0], '')
            cdoutput,cderror=ssh(address,'cd %s' %self.working_folder.get(),pw, cipher_suite) 
            if 'No such file or directory' in cderror:
                coutput=ssh(address,'mkdir -p %s' %self.working_folder.get(),pw, cipher_suite) 
                outputtext.insert('end-1c', 'Created working directory %s.\n' %self.working_folder.get())
            else:
                outputtext.insert('end-1c','Working in %s.\n' %self.working_folder.get())
                         
            param_files=[]
            for fname in input_files:
                param_files.append(fname.replace('.cell', '.param'))
            for j in sites:
                folder=os.path.join(cells.get(),'site%i' %j)
                if os.path.isdir(folder):
                    shutil.rmtree(folder)
                os.mkdir(folder)
                copyfile(input_files[j-1],os.path.join(folder,os.path.basename(input_files[j-1])))
                # add k points if specified
                write_to_cell(self.custom_cell,os.path.join(folder,os.path.basename(input_files[j-1])),self.kpoints.get())
                param_files2=os.path.join(folder,os.path.basename(param_files[j-1]))
                if test_custom():
                    ptext=self.custom_p 
                else:
                    ptext=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get())   
                with open(param_files2,'w') as f:
                    f.write(ptext)
                write_script(self.mode.get(), self.submission_sys, folder, jobname.get(), num_cores.get(), os.path.basename(param_files2.replace('.param', '')),self.cores_per_node)          
                scp(folder, '%s@%s:%s' %(self.username.get(), self.hpc_address, 
                self.working_folder.get()), pw, cipher_suite, recursive=True)
                id_sdb=submit_job(self.submission_sys,self.username.get(),self.working_folder.get(),j,address,pw, cipher_suite)
                outputtext.insert("end-1c", 'Submitted job %s for Site %i.\n' %(id_sdb,j))
                job_ids.append(id_sdb)     
            for id in job_ids:
                self.jobs.append(id)
            for site in sites:
                self.sites.append(site)
            save_jobs_func()
     
        def check_queue(): 
            if self.password==None:
                    self.password=get_password()
            pw=self.password
            if self.jobs:
                #clear lingering empty jobs
                while([] in self.jobs): 
                    self.jobs.remove([])
                while([] in self.sites):
                    self.sites.remove([])
                address='%s@%s' %(self.username.get(), self.hpc_address)
                resub_jobs=[]
                resub_sites=[]
                queue_str=copy_queue(self.submission_sys,self.username.get(),address,pw,cipher_suite)
                for i,id in enumerate(self.jobs):
                    inqueue,job_status=search_queue(id,queue_str,self.submission_sys)
                    if inqueue==True:
                       if job_status=='waiting':
                           outputtext.insert("end-1c",'Job %s for Site %i still in queue.\n' %(id,self.sites[i]))
                       elif job_status=='running':
                           outputtext.insert("end-1c",'Job %s for Site %i running.\n' %(id,self.sites[i]))
                    else:
                       results=os.path.join(os.path.dirname(cells.get()),'results')
                       if os.path.isdir(results)==False:
                           os.mkdir(results)
                       castep_file='%s/site%i/%s_%i.castep' %(self.working_folder.get(),self.sites[i],
                       self.rootname,self.sites[i])
                       outcell=castep_file.replace('.castep','-out.cell')
                       output,error=ssh(address, 'ls %s' %outcell, pw, cipher_suite)
                       if output.rstrip()==outcell:
                           outputtext.insert("end-1c",'Job for Site %i finished.\n' %self.sites[i])
                           scp('%s@%s:%s' %(self.username.get(), self.hpc_address, castep_file), 
                           results, pw, cipher_suite, recursive=True)
                           scp('%s@%s:%s' %(self.username.get(), self.hpc_address, outcell), 
                           results, pw,cipher_suite, recursive=True)
                           outputtext.insert("end-1c",'Output files copied to %s.\n' %results)
                           self.jobs[i]=[]
                           self.sites[i]=[]
                       else:
                           outputtext.insert("end-1c",'Job %i for Site %i is no longer in queue, but did not finish.\n' %(self.jobs[i],self.sites[i]))
                           if self.resub.get()==True:
                               id_sdb=submit_job(self.submission_sys,self.username.get(),self.working_folder.get(), self.sites[i],address,pw, cipher_suite)
                               outputtext.insert("end-1c", 'Submitted job %s for continuation of Site %i.\n' %(id_sdb,self.sites[i]))
                               self.jobs[i]=[]
                               tmp_site=self.sites[i]
                               self.sites[i]=[]
                               resub_jobs.append(id_sdb)
                               resub_sites.append(tmp_site)
                               
                for id in resub_jobs:
                    self.jobs.append(id)
                for site in resub_sites:
                    self.sites.append(site)
                
                #clear empty jobs
                while([] in self.jobs): 
                    self.jobs.remove([])
                while([] in self.sites):
                    self.sites.remove([])
                    
            else:
                outputtext.insert('end-1c','No jobs running!\n')
            
            save_jobs_func()
                    
        ttk.Frame.__init__(self, *args, **kwargs)
        
        cells_lbl=Label(self, text="Input files:")
        cells_lbl.grid(row=0, column=0, sticky=W)
        cells = Entry(self)
        cells.grid(row=1, column=0, sticky=W)
        cells_ttp = ToolTip(cells_lbl, "folder containing input cell files")
        b3 = ttk.Button(self, text="load", width=10, command=load_dir).grid(row=2, column=0, sticky=W)

        self.mode=StringVar(self)
        mode_options=["local"]+hpc_names
        Label(self, text="Mode").grid(row=3, column=0, sticky=W)
        w_mode=ttk.OptionMenu(self, self.mode, mode_options[0], *mode_options)
        w_mode.grid(row=4, column=0, sticky=W)
 
        param_frame = LabelFrame(self, text="Parameters", pady=2)
        param_frame.grid(row=0, column=1, columnspan=1, rowspan=7,sticky='WE', \
             padx=2, pady=2, ipadx=2, ipady=2)
             
        charge=StringVar(self)
        charge_options=['mu+','muonium']
        Label(param_frame, text="Muon charge state").grid(row=0, column=0, sticky=W)
        w_charge=ttk.OptionMenu(param_frame, charge, charge_options[0], *charge_options)
        w_charge.grid(row=1, column=0, sticky=W)
        
        self.xc=StringVar(self)
        xc_options=['LDA','PBE']
        Label(param_frame, text="Exchange-correlation functional").grid(row=2, column=0, sticky=W)
        w_xc = ttk.OptionMenu(param_frame, self.xc, xc_options[0], *xc_options)
        w_xc.grid(row=3, column=0, sticky=W)

        spin_pol = IntVar()
        Checkbutton(param_frame, text="spin-polarized?", variable=spin_pol).grid(row=4, column=0, sticky=W)
        
        self.basis_prec=StringVar(self)
        bp_options=['COARSE','MEDIUM','FINE','PRECISE','EXTREME']
        Label(param_frame, text="Basis precision").grid(row=5, column=0, sticky=W)
        w_bp = ttk.OptionMenu(param_frame, self.basis_prec, bp_options[2], *bp_options)
        w_bp.grid(row=6, column=0, sticky=W)
        
        self.solver=StringVar(self)
        solver_options=['Density mixing','edft']
        Label(param_frame, text="SCF Solver").grid(row=0, column=2, sticky=W)
        w_bp = ttk.OptionMenu(param_frame, self.solver, solver_options[0], *solver_options)
        w_bp.grid(row=1, column=2, sticky=W)
        
        k_label=Label(param_frame, text="k-point grid")
        k_label.grid(row=2, column=2, sticky=W)
        self.kpoints=SimpleTableInput(param_frame, 1, 3)
        self.kpoints.grid(row=3, column=2, sticky=W)
        self.kpoints.set('')
        param_frame.grid_columnconfigure(1, weight=1)
        
        adv=Label(param_frame, text="Advanced options")
        adv.grid(row=4,column=2,sticky=W)
        
        def test_custom():
            tmp_param=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get())
            if tmp_param ==self.old_param:
                return True
            else:
                return False
            
        def input_custom():
            tmp_param=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get())
            if tmp_param !=self.old_param:
                self.custom_p=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get())
                self.old_param=param_text(charge.get(),self.xc.get(), spin_pol.get(), self.basis_prec.get(),self.solver.get())
                self.custom_p=CustomDialog(self, self.custom_p,"Custom parameters").show()
            else:
                self.custom_p=CustomDialog(self, self.custom_p,"Custom parameters").show()
        
        self.custom_param=ttk.Button(param_frame, text='Custom parameters', command=input_custom)
        self.custom_param.grid(row=5, column=2, sticky=W)
        
        def input_custom_cell():
            self.custom_cell=CustomDialog(self, self.custom_cell,"Cell keywords").show()
                    
        self.cell_kw=ttk.Button(param_frame, text='Cell keywords', command=input_custom_cell)
        self.cell_kw.grid(row=6, column=2, sticky=W)
        
        
        castep_comm=StringVar(self)
        castep_options=['castep.mpi','castep.serial']
        castep_comm_label=Label(self, text="CASTEP command")
        castep_comm_label.grid(row=8, column=1, sticky=W)
        castep_ttp = ToolTip(castep_comm_label, "Choose between parallel or serial version of CASTEP")
        w_castep=ttk.OptionMenu(self, castep_comm, castep_options[0], *castep_options)
        w_castep.grid(row=9, column=1, sticky=W)
        
        inputnums=TextField(self, "sites to run", 5, 0, '',
        'Sites to run')
        run=ttk.Button(self, text='Run calculations', command=run_calc)
        run.grid(row=8, column=0, sticky=W, pady=4)

        outputtext = tkst.ScrolledText(self, height=10, width=100)
        outputtext.grid(column=0, row=12, columnspan=2)
        
        self.grid_rowconfigure(13, weight=1)
           
        cluster_frame = LabelFrame(self, text="Manage jobs on HPC cluster", pady=2)
        cluster_frame.grid(row=17, columnspan=2, sticky='WE', \
             padx=2, pady=2, ipadx=2, ipady=2)
        
        run=ttk.Button(cluster_frame, text='Check queue', command=check_queue)
        run.grid(row=14, column=2, sticky=W, pady=4)
            
        self.working_folder=TextField(cluster_frame, "Working directory", 14, 0, '',
                      'Folder to run calculations from')
        
        self.username=TextField(cluster_frame, "username", 16, 0, '',
                         'HPC cluster username')
        jobname=TextField(cluster_frame, "Job name", 14, 1, 'job',
                         'Jobs will appear as jobname${i}')
        num_cores=TextField(cluster_frame, "Number of processes", 16, 1, '24',
        'Number of processes to use')
        
        def load_jobs():
            pickle_file=os.path.join(cells.get(),'jobs.pkl')
            with open(pickle_file, 'rb') as f:  # Python 3: open(..., 'rb')
                self.sites=pickle.load(f)
                self.jobs=pickle.load(f)
                self.rootname=pickle.load(f)
                self.username.replace(pickle.load(f))
                self.working_folder.replace(pickle.load(f))
                jobname.replace(pickle.load(f))
                num_cores.replace(pickle.load(f))
            outputtext.insert("end-1c","Loaded job information from %s.\n" %pickle_file)

        def save_jobs_func():    
            pickle_file=os.path.join(cells.get(),'jobs.pkl')
            with open(pickle_file, 'wb') as f:  # Python 3: open(..., 'wb')
                pickle.dump(self.sites, f)
                pickle.dump(self.jobs, f)
                pickle.dump(self.rootname, f)
                pickle.dump(self.username.get(), f)
                pickle.dump(self.working_folder.get(), f)
                pickle.dump(jobname.get(), f)
                pickle.dump(num_cores.get(),f)
            outputtext.insert("end-1c","Saved job information to %s.\n" %pickle_file)

        load_jobs=ttk.Button(cluster_frame, text='Load jobs', command=load_jobs)
        load_jobs.grid(row=17, column=3, sticky=W, pady=4)
        
        jobid=TextField(cluster_frame, "Job ID", 15, 2, '',
        'Enter ID of job to cancel')
        
        self.resub = IntVar()
        Checkbutton(cluster_frame, text="resubmit if not finished", variable=self.resub).grid(row=14, column=3, sticky=W)
        
        def cancel_job():
            if self.password==None:
                    self.password=get_password()
            jobids=get_numbers(jobid.get(),outputtext)
        
            if self.submission_sys=='slurm':
                cancel_comm='scancel'
            if self.submission_sys=='pbs':
                cancel_comm='qdel'

            for id0 in jobids:
                output,error=ssh('%s@%s' %(self.username.get(), self.hpc_address),
                '%s %s' %(cancel_comm,id0),self.password, cipher_suite)
                for i,id in enumerate(self.jobs):
                    if id==int(id0):
                        self.jobs.pop(i)
                        self.sites.pop(i)
                        outputtext.insert('end-1c','Cancelled job %s.\n' %id0)
            
            save_jobs_func()
                    
        def cancel_all():
            if self.submission_sys=='slurm':
                cancel_comm='scancel'
            if self.submission_sys=='pbs':
                cancel_comm='qdel'
                
            for i,id in enumerate(self.jobs):
                output=ssh('%s@%s' %(self.username.get(), self.hpc_address), 
                '%s %s' %(cancel_comm,id),self.password, cipher_suite)
                
            self.jobs=[]
            self.sites=[]  
            outputtext.insert('end-1c','Cancelled all jobs.\n') 
            save_jobs_func() 
            
        cancel=ttk.Button(cluster_frame, text='Cancel job(s)', command=cancel_job)
        cancel.grid(row=16, column=3, sticky=W, pady=4)
        
        cancel_all=ttk.Button(cluster_frame, text='Cancel all', command=cancel_all)
        cancel_all.grid(row=17, column=2, sticky=W, pady=4)
        
        def cancel_loc():
            global castep_calc
            if castep_calc==[]:
               outputtext.insert('end-1c','No local calculations submitted!\n')
               return
            poll=castep_calc.poll()
            if poll is None:
               castep_calc.terminate()
               castep_calc.wait()
               outputtext.insert('end-1c','Cancelled running calculation.\n')
            else:
               outputtext.insert('end-1c','No calculations running locally.\n')
        
        cancel_local=ttk.Button(self, text='Cancel local calculation', command=cancel_loc)
        cancel_local.grid(row=9, column=0, sticky=W, pady=4)
        
        ttk.Button(self, text='Quit', command=self.quit).grid(row=22, column=0, sticky=W, pady=4)


