"""Method of calculating the correction to the dipolar field due to distortions 
induced by the muon"""

import os

from ase import io
from ase import Atoms
from ase.build.supercells import make_supercell
from ase.geometry.geometry import get_distances

from muesr.core import Sample
from muesr.engines.clfc import find_largest_sphere, locfield, dipten
from muesr.i_o import load_cif

import math
import numpy as np
import numpy.linalg as LA

def atoms_to_sample(cif_file, atoms):
    # temporary cif file to convert ASE atoms objects to Sample
    tmp_cif=os.path.join(os.path.dirname(cif_file),'tmp.cif')
    io.write(tmp_cif,atoms)
    sample=Sample()
    load_cif(sample,tmp_cif)
    os.remove(tmp_cif)
    return sample
    
def recentre_cell(muon_pos,struct):    
    shifted_pos=[]
    for atom_num, atom_pos in enumerate(struct.get_scaled_positions()):
        shifted_pos.append(atom_pos-muon_pos+[0.5,0.5,0.5])
    struct.set_scaled_positions(shifted_pos)  
    
def reorder_atoms(struct1,struct2):
    # reorder atoms in struct2 to be in the same order as in struct1
    pos_mat,dist_mat=get_distances(struct1.get_positions(),struct2.get_positions(),
    cell=struct1.get_cell(),pbc=True)
    new_positions=struct2.get_positions()
    for i,dist_i in enumerate(dist_mat):
        j=np.argmin(dist_i)
        # check that the atomic species are the same, if not find next smallest distance
        while struct1.get_atomic_numbers()[i]!=struct2.get_atomic_numbers()[j]:
            dist_i[j]=np.inf
            j=np.argmin(dist_i)
        # make all members of column j infinite so that atom j can only be mapped to a 
        # single atom in structure 1
        dist_mat[:,j]=np.inf 
        new_positions[i]=struct2.get_positions()[j]
    struct2.set_positions(new_positions)
               
def distortion_correction(mu_struct,muon_pos,indices,cif_file,k,mag_array, select):
    r_corr=[]
    D_corr=[] 
    for index in indices:
        dist_struct=mu_struct.valid_struct[index].copy()
     
        # need to determine supercell matrix
        single_cell=io.read(cif_file)
        supercell_m=dist_struct.get_cell() @ LA.inv(single_cell.get_cell())
        supercell_mat=(np.round(supercell_m)).astype(int)
        supercell_mupos=mu_struct.muon_sites_supercell[index]     
        scell_species=make_supercell(single_cell, supercell_mat) 
        single_cell.set_atomic_numbers([x+1 for x in range(len(single_cell.get_atomic_numbers()))])
        scell=make_supercell(single_cell, supercell_mat)
                    
        # generate larger cell to negate effects of pbc
        super_duper_cell=scell.copy()
    
        # reorder atoms such that they are in the same order in both cells
        reorder_atoms(scell_species,dist_struct)             
        
        super_duper_cell.set_cell(2*scell.get_cell())
        super_duper_cell.set_positions(scell.get_positions())
          
        # recenter both cells on the muon position
        recentre_cell(supercell_mupos,dist_struct)
        recentre_cell(0.5*supercell_mupos,super_duper_cell)
               
        # translate atoms in the larger cell by translation vectors such that they are
        # all as close as possible to those in the distorted cell
        new_pos=[]
        for atom_num, atom_pos in enumerate(super_duper_cell.get_scaled_positions()):
            trans_pos=[]
            disp=[]
            for l in [-1,0,1]:
                for m in [-1,0,1]:
                    for n in [-1,0,1]:
                        pos2=atom_pos+[.5*l,.5*m,.5*n]
                        trans_pos.append(pos2)
                        disp.append(np.linalg.norm(scell.get_cell() @ (2*pos2
                        -dist_struct.get_scaled_positions()[atom_num]-[0.5,0.5,0.5])))
            new_pos.append(trans_pos[np.argmin(disp)])
        super_duper_cell.set_scaled_positions(new_pos)

        k_vec=np.array([float(k[0][0]), float(k[0][1]), float(k[0][2])])
   
        #generate mag_array for simulation cell
        relaxed_mag_array=[]
        for i, atom_no in enumerate(scell.get_atomic_numbers()):
            R_vec=(scell.get_scaled_positions()[i]-scell.get_scaled_positions()[atom_no-1])
            phase=2*math.pi*np.dot(k_vec,R_vec @ supercell_m)
            relaxed_mag_array.append([np.exp(-phase*1.j)*x for x in mag_array[atom_no-1]])
                    
        # save mag_array without muon for later
        sim_mag_array=relaxed_mag_array.copy()
             
        # add moment on muon (=0) to last position of array
        relaxed_mag_array.append([0.+0.j, 0.+0.j, 0.+0.j])
         
        distorted_xtal=atoms_to_sample(cif_file, dist_struct)     
       	
        # Define magnetic structure
        distorted_xtal.new_mm()
        distorted_xtal.mm.k = np.array([0, 0, 0])
        distorted_xtal.mm.fc=np.array(relaxed_mag_array)
        	
        # add muon, always at (0.5, 0.5 0.5) since we shifted the cell
        distorted_xtal.add_muon([0.5,0.5,0.5])
            
        # repeat for undistorted cell    
        xtal2=atoms_to_sample(cif_file, super_duper_cell)
                
        xtal2.new_mm()
        xtal2.mm.k = np.array([0, 0, 0])
        xtal2.mm.fc=np.array(sim_mag_array)
        xtal2.add_muon([0.5,0.5,0.5])
        
        if select=='field':        
            # assume that a radius of 1000 A will include all of the atoms in the cell
            r_dist=locfield(distorted_xtal, 's', [1,1,1], 1000)
            r_undist=locfield(xtal2, 's', [1,1,1], 1000)
            r_corr.append(r_dist[0].D-r_undist[0].D)
      
        if select=='tensor':        
            # assume that a radius of 1000 A will include all of the atoms in the cell
            D_dist=dipten(distorted_xtal, [1,1,1], 1000)
            D_undist=dipten(xtal2, [1,1,1], 1000)
            D_corr.append(D_dist[0]-D_undist[0])
    
    if select=='field':
        return r_corr
    if select=='tensor':
        return D_corr
