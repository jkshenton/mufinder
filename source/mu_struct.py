#!/usr/bin/python

"""Class defining object to hold results of clustering analysis"""

class mu_struct:
    
    def __init__(self):
        self.input_struct=[]
        self.new_label=''
        self.valid_struct=[None]
        self.muon_sites=[None]
        self.muon_sites_supercell=[]
        self.shifted=[None]
        self.clust_composition=[]