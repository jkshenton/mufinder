#!/usr/bin/python

import os
import glob
import spglib as spg
import numpy as np
from argparse import ArgumentParser
from ase import io
from ase.io import write
from ase import Atoms
from ase.calculators.singlepoint import SinglePointCalculator
from ase.geometry import cell_to_cellpar
from ase.spacegroup import crystal
from soprano.collection import AtomsCollection
from soprano.properties.basic import CalcEnergy
from custom_view import *
import math
from numpy import linalg as LA

from ase.visualize import view

from clustering.components import *
from clustering.kmeans import kmeans_clustering
from clustering.shift import add_sites, shift_sites
from clustering.displacements import *

# function to interpret label used for muon
def muon_label(filename,outputtext):

    with open(filename) as myfile:
        if 'H:mu' in myfile.read():
            return 'H:mu'

    with open(filename) as myfile:
        if 'H ' in myfile.read():
            return 'H'
    
    outputtext.insert("end-1c",'Muon not found. Aborting...\n')
    return 0

# function to relabel muon
def muon_relabel(output_files, old_label, new_label):
    for file in output_files:
        f1 = open(file, 'r')
        f2 = open(os.path.join(os.path.dirname(file),'tmp_%s' %os.path.basename(file)), 'w')
        for line in f1:
            f2.write(line.replace(old_label, new_label))
        f1.close()
        f2.close()

def cluster(algorithm, shift, mu, params, out_path, outputtext, aout, use_max_disp, max_disp):

    # Loading CASTEP output files
    cout_files = glob.glob(os.path.join(out_path, '*-out.cell'))
    castep_files=glob.glob(os.path.join(out_path, '*.castep'))

    # Get label for muon if not specified by user
    if mu=='':
        mu=muon_label(cout_files[0],outputtext)
        if mu==0:
            exit()

    def load_castep(fname):
        a = io.read(fname)
        # Create a calculator to store the energy
        a_cast = io.read(fname.replace('-out.cell', '.castep'))
        spcalc = SinglePointCalculator(a, 
                                   energy=a_cast.get_potential_energy())
        a.set_calculator(spcalc)
        return a
        
    # ASE cannot properly interpret H:mu, here we relabel the muon to fix this
    if mu=='H:mu':
        aout.new_label='Pu' # Hopefully your sample doesn't contained plutonium!
        muon_relabel(cout_files, mu, 'Pu') 
        muon_relabel(castep_files, mu, 'Pu')
        cout_files=[os.path.join(os.path.dirname(file),'tmp_%s' %os.path.basename(file)) for file in cout_files]
        castep_files=[os.path.join(os.path.dirname(file),'tmp_%s' %os.path.basename(file)) for file in castep_files]
        
        outputtext.insert("end-1c",'Loading structures...\n')
        cout_atoms = [load_castep(f) for f in cout_files]
        
        # delete temporary files
        for file in cout_files:
            os.remove(file)
        for file in castep_files:
            os.remove(file)

    else:
        aout.new_label=mu
        outputtext.insert("end-1c",'Loading structures...\n')
        cout_atoms = [load_castep(f) for f in cout_files]

    # Load collection of results
    resultsColl = AtomsCollection(cout_atoms)
    outputtext.insert("end-1c",'Loaded {0} structures.\n'.format(len(resultsColl)))
    # Extract the list of energies and append it to the collection
    E = np.array(CalcEnergy.get(resultsColl))
    resultsColl.set_array('E', E)
    resultsColl = resultsColl.sorted_byarray('E') # Sort by energy


    # Extract fractional coordinates of muon sites and translate these to coordinates in 
    # the conventional cell if a supercell has been used
    muon_sites=[]
    muon_sites_supercell=[]
    valid_struct=[]
    for structure in resultsColl.structures:
        if use_max_disp==True:
            max_disp_struct=max_displacement(aout.input_struct,structure)
        else:
            max_disp_struct=0
            max_disp=1
        scell_scale=structure.get_cell() @ LA.inv(aout.input_struct.get_cell())
        muon_pos = [structure.get_scaled_positions()[i] for i in range(len(structure.get_atomic_numbers())) if structure.get_chemical_symbols()[i]==aout.new_label]
        if len(muon_pos)>0 and max_disp_struct<=float(max_disp): # check for 'broken' output files and maximum allowed displacement
            muon_sites_supercell.append(muon_pos[0])
            muon_sites.append([math.modf(x)[0] for x in (muon_pos[0] @ scell_scale)])
            valid_struct.append(structure)
   
    aout.valid_struct=valid_struct
    aout.muon_sites=muon_sites
    aout.muon_sites_supercell=muon_sites_supercell       
    
    if algorithm=='connected components':
        aout.clust_composition=connected_components(aout, resultsColl, params, outputtext)
    elif algorithm=='k-means':
        aout.clust_composition=kmeans_clustering(aout, resultsColl, params)
    outputtext.insert("end-1c",'Clusters are: %s\n' %aout.clust_composition)
    
    if shift==True:
        outputtext.insert("end-1c",'Shifting muon positions to occupy smaller subspace of the unit cell...\n') 
        aout.shifted=shift_sites(aout)
    else:
        aout.shifted=add_sites(aout)
        
    outputtext.insert("end-1c",'Done! \n')
