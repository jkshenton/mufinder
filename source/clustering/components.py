#!/usr/bin/python

import os
import glob
import spglib as spg
import numpy as np
from argparse import ArgumentParser
from ase import io
from ase.io import write
from ase import Atoms
from ase.calculators.singlepoint import SinglePointCalculator
from ase.geometry import cell_to_cellpar
from ase.spacegroup import crystal
from soprano.collection import AtomsCollection
from soprano.properties.basic import CalcEnergy
from custom_view import *
import math
from numpy import linalg as LA

import networkx as nx

from clustering.shift import gen_sym_equiv, gen_single_muon

def connected_components(aout, resultsColl, params, outputtext):
    tol=float(params)
    input_struct=aout.input_struct
    new_label=aout.new_label
    
    # construct distance matrix  
    outputtext.insert("end-1c",'Constructing distance matrix...\n')
    dist_mat=[]
    for j, site_j in enumerate(aout.muon_sites):    
            replica_j=gen_sym_equiv(site_j, input_struct, 'H')
            for i,site_i in enumerate(aout.muon_sites):
                replica_i=gen_single_muon(site_i, input_struct, 'O')
                combined=replica_i+replica_j
                atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
                if combined.get_chemical_symbols()[x]=='O']
                atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
                if combined.get_chemical_symbols()[y]=='H']
                distances=[]
                for k in range(len(atoms2)):
                    distances.append(np.min(combined.get_distances(atoms1, atoms2[k], mic=True)))
                dist_mat.append(np.min(distances))

    dist_mat=np.reshape(dist_mat, (len(aout.muon_sites), len(aout.muon_sites)))

    # construct adjacency matrix
    adj=dist_mat < tol
    adj[np.diag_indices_from(adj)]=0
    graph=nx.from_numpy_matrix(adj.astype(int))

    # find connected subgraphs
    outputtext.insert("end-1c",'Finding connected subgraphs...\n')
    subgraphs=[]
    for h in nx.connected_component_subgraphs(graph):
            subgraphs.append(list(h.nodes)) 

    return subgraphs
    
