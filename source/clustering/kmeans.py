#!/usr/bin/python

from soprano.analyse.phylogen import PhylogenCluster, Gene, GeneDictionary

def kmeans_clustering(mu_struct, resultsColl, params):
    
    num_clusters=int(params)
    
    # Cluster according to energy and bond order parameters around the muon
    geneE = Gene('energy')
    geneBOrd = Gene('bond_order_pars', params={'s1': mu_struct.new_label})

    clustObj = PhylogenCluster(resultsColl, genes=[geneBOrd, geneE])
    k_clust_inds, k_clust_composition = clustObj.get_kmeans_clusters(num_clusters)
 
    return sorted([l.tolist() for l in k_clust_composition], key=lambda x: x[0])
