# MuFinder

A program to classify and analyse muon stopping sites.  This software is in open beta, I would be grateful for any bug reports or feature requests.

The python code is included in the 'source' folder.

Installation instructions and usage instructions can be found in 'manual.pdf'.  The executables should be downloaded as separate files and do not function correctly when downloading the whole master branch. 

The files needed to work through the examples in the manual can be found in the 'examples' folder.
